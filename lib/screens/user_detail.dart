import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:intl/intl.dart';
import '../models/user.dart';
import '../models/account.dart';
import '../database/repository.dart';
import '../screens/account_list.dart';

class UserDetail extends StatefulWidget {

      User user;
      String appBarTitle;
      

      UserDetail(this.user, this.appBarTitle);

      @override
      State<StatefulWidget> createState() {
      return UserDetailState(this.user, this.appBarTitle);
  }
}

class UserDetailState extends State<UserDetail> {

      var _genders = ['Female', 'Male'];

      Repository repository = Repository();

      String appBarTitle;

      var _isButtonVisible = true;

      UserDetailState(this.user, this.appBarTitle){
            if(user.birthDate == ''){
                  user.birthDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
                  print(DateFormat('yyyy-MM-dd').format(DateTime.now()));
            }

            if(appBarTitle == "Add User"){

                  _isButtonVisible = false;
            }
            else{
                  _isButtonVisible = true;
            }
            
      }

      TextEditingController nameController = TextEditingController();

      DateTime _date = DateTime.now();

      var formatter = new DateFormat('yyyy-MM-dd');
      
      User user;

      Future<Null> _selectDate(BuildContext context) async{

            
            String formatted = formatter.format(_date);
            print(formatted);
            if(user.birthDate != ''){
                  _date = DateTime.parse(user.birthDate);
            }
            else{
                  user.birthDate = formatter.format(_date);
            }
          
            
            final DateTime picked = await showDatePicker(

                  context: context,
                  initialDate: _date,
                  firstDate: new DateTime(1960),
                  lastDate: new DateTime(2019),
            );
      
            if(picked !=null && picked != _date) {
                  debugPrint('Date selected: ${_date.toString()}');
                  setState(() {
                        _date = picked;
                        user.birthDate = _date.toString();          
                  });
            }
      } 

      void _showAlertDialog(String title, String message){
                  AlertDialog alertDialog = AlertDialog(
                        title: Text(title),
                        content: Text(message),
                  );
                  showDialog(
                        context: context,
                        builder: (_) => alertDialog
                  );
            }

      @override
      Widget build(BuildContext context) {

            TextStyle textStyle = Theme.of(context).textTheme.title;
            
            nameController.text = user.name;

            void _save() async {


                  debugPrint('-------Attempting to post User ${user.name}.....');
                  await repository.insertUser(user);
                  debugPrint('Operation FINISHED');

                  moveToLastScreen();
            }

            void _update() async {

                  debugPrint('-------Attempting to Edit User ${user.name}.....');
                  await repository.updateUser(user);
                  debugPrint('Operation FINISHED');

                  moveToLastScreen();
            }

            void _delete() async {
                    


                  // Case 1: If user is trying to delete the NEW USER i.e. he has come to
                  // the detail page by pressing the FAB of NoteList page.
                    if(user.id == null) {
                          _showAlertDialog('Status', 'No Note was Deleted');
                          return;
                    }

                  // Case 2: User is trying to delete the old note that already has a valid ID.
                    int result = await repository.deleteUser(user);

                    if(result != 0){
                          _showAlertDialog('Status', 'Note Deleted Successfully');
                    }
                    else {
                          _showAlertDialog('Status', 'Error Ocurred while Deleting Note');
                    }

                    moveToLastScreen();
            }

            

            return WillPopScope(

            onWillPop: () {

              moveToLastScreen();
            },
            child: Scaffold(
                appBar: AppBar(
                  title: Text(appBarTitle),
                  leading: IconButton(icon: Icon(Icons.arrow_back),
                  onPressed:  () {
                    moveToLastScreen();
                  },
                  ),
                ),
                body: Padding(
                  padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                  child: ListView(children: <Widget>[
                    ListTile(
                      title: DropdownButton(
                          items: _genders.map((String dropDownStringItem) {
                            return DropdownMenuItem<String>(
                              value: dropDownStringItem,
                              child: Text(dropDownStringItem),
                            );
                          }).toList(),
                          style: textStyle,
                          value: user.getFullGender(),
                          onChanged: (valueSelectedByUser) {
                            setState(() {
                              debugPrint('User selected $valueSelectedByUser');
                              user.updateGenderWithString(valueSelectedByUser);
                            });
                          }),
                    ),

                    //Second Element

                    Padding(
                          padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                          child: TextField(
                                controller: nameController,
                                style: textStyle,
                                onChanged: (value) {
                                      debugPrint('Something changed in title Text Field');
                                      user.updateName(nameController.text);
                                },
                                decoration: InputDecoration(
                                      labelText: 'Full Name',
                                      labelStyle:  textStyle,
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5.0)
                                      )
                                ),

                          ),
                    ),

                    //Third Element
                    Padding(
                          padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                          child: Text('Date of Birth : ${formatter.format(_date)}'),
                  
                    ),

                    Padding(

                            padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
            
                            child: RaisedButton(
                                  color: Theme.of(context).primaryColorDark,
                                  textColor: Theme.of(context).primaryColorLight,
                                  child: Text(
                                  'Change Date of Birth',
                                  textScaleFactor: 1.5,
                              ),
                              onPressed: () {
                                    setState(() {

                                          debugPrint("Save button clicked");
                                          _selectDate(context);
                                    });
                              },
                            ),

                          
                  
                              ),

                    //Fourth Element

                    Padding(
                      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                              color: Theme.of(context).primaryColorDark,
                              textColor: Theme.of(context).primaryColorLight,
                              child: Text(
                                'Save',
                                textScaleFactor: 1.5,
                              ),
                              onPressed: () {
                                setState(() {

                                  debugPrint("Save button clicked");
                                  if(user.id == null){
                                        _save();
                                  }
                                  else{
                                        _update();
                                  }
                                  
                                });
                              },
                            ),

                          ),
                          
                          Container(width: 5.0,),

                          Expanded(
                            child: RaisedButton(
                              color: Theme.of(context).primaryColorDark,
                              textColor: Theme.of(context).primaryColorLight,
                              child: Text(
                                'Delete',
                                textScaleFactor: 1.5,
                              ),
                              onPressed: () {
                                setState(() {

                                  debugPrint("Delete button clicked");
                                  _delete();

                                });
                              },
                            ),

                          ),
                        ],
                      ),
                    ),

                  ]),
                ),

                        floatingActionButton:new Opacity(
                        opacity: _isButtonVisible ? 1.0 : 0.0, 
                              child: FloatingActionButton(
                              onPressed: () {
                                    debugPrint('FAB clicked');
                                    navigateToAccounts(user);
                              },
                              tooltip: 'Add Note',

                              child: Icon(Icons.payment),
                        
                              ),
                        ),
    
          )
          );
          }


      void moveToLastScreen() {
        Navigator.pop(context, true);
      }
      void navigateToAccounts(User user) async {
          bool result = await Navigator.push(context, MaterialPageRoute(builder: (context) {
            return AccountList(user);
          }));


          // if(result == true){
          //   updateListView();
          // }
      }



}

