import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/account.dart';
import '../database/repository.dart';

class AccountDetail extends StatefulWidget {

      Account account;
      String appBarTitle;

      AccountDetail(this.account, this.appBarTitle);

      @override
      State<StatefulWidget> createState() {
      return AccountDetailState(this.account, this.appBarTitle);
  }
}

class AccountDetailState extends State<AccountDetail> {

      var _types = ['Regular', 'Savings'];
      Account account;
      Repository repository = Repository();

      String appBarTitle;

      

      AccountDetailState(this.account, this.appBarTitle);

      TextEditingController nameController = TextEditingController();


      void _showAlertDialog(String title, String message){
                  AlertDialog alertDialog = AlertDialog(
                        title: Text(title),
                        content: Text(message),
                  );
                  showDialog(
                        context: context,
                        builder: (_) => alertDialog
                  );
            }

      @override
      Widget build(BuildContext context) {

            TextStyle textStyle = Theme.of(context).textTheme.title;
            
            nameController.text = account.getAccountString();

            void _save() async {


                  debugPrint('-------Attempting to post Account ${account.id}.....');
                  await repository.insertAccount(account);
                  debugPrint('Operation FINISHED');

                  moveToLastScreen();
            }

            void _update() async {

                  debugPrint('-------Attempting to Edit Account ${account.id}.....');
                  await repository.updateAccount(account);
                  debugPrint('Operation FINISHED');

                  moveToLastScreen();
            }

            void _delete() async {
                    


                  // Case 1: If user is trying to delete the NEW Account i.e. he has come to
                  // the detail page by pressing the FAB of AccountList page.
                    if(account.id == null) {
                          _showAlertDialog('Status', 'No Account was Deleted');
                          return;
                    }

                  // Case 2: User is trying to delete the old note that already has a valid ID.
                    int result = await repository.deleteAccount(account);

                    if(result != 0){
                          _showAlertDialog('Status', 'Account Deleted Successfully');
                    }
                    else {
                          _showAlertDialog('Status', 'Error Ocurred while Deleting Account');
                    }

                    moveToLastScreen();
            }

            

            return WillPopScope(

            onWillPop: () {

              moveToLastScreen();
            },
            child: Scaffold(
                appBar: AppBar(
                  title: Text(appBarTitle),
                  leading: IconButton(icon: Icon(Icons.arrow_back),
                  onPressed:  () {
                    moveToLastScreen();
                  },
                  ),
                ),
                body: Padding(
                  padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                  child: ListView(children: <Widget>[
                    ListTile(
                      title: DropdownButton(
                          items: _types.map((String dropDownStringItem) {
                            return DropdownMenuItem<String>(
                              value: dropDownStringItem,
                              child: Text(dropDownStringItem),
                            );
                          }).toList(),
                          style: textStyle,
                          value: account.getAccountString(),
                          onChanged: (valueSelectedByUser) {
                            setState(() {
                              debugPrint('User selected $valueSelectedByUser');
                              account.updateAccountId(valueSelectedByUser);
                            });
                          }),
                    ),

                    //Second Element

                    Padding(
                          padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                          child: new TextField(
                                      decoration: InputDecoration(
                                            labelText: 'Total Amount',
                                            labelStyle:  textStyle,
                                            border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5.0)
                                            ),
                                            ),
                                            keyboardType: TextInputType.number,
                                            style: textStyle,
                                            onChanged: (value){
                                                  debugPrint('Something changed in title Text Field');
                                                  account.amount = double.parse(value);
                                            },
                          ),
    
                    ),

                    //Fourth Element

                    Padding(
                      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                              color: Theme.of(context).primaryColorDark,
                              textColor: Theme.of(context).primaryColorLight,
                              child: Text(
                                'Save',
                                textScaleFactor: 1.5,
                              ),
                              onPressed: () {
                                setState(() {
                                  
                                  debugPrint("Save button clicked");
                                  if(account.id == null){
                                 
                                        account.creationDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
                                        print(DateFormat('yyyy-MM-dd').format(DateTime.now()));
            
                                        _save();
                                  }
                                  else{
                                        _update();
                                  }
                                  
                                });
                              },
                            ),

                          ),
                          
                          Container(width: 5.0,),

                          Expanded(
                            child: RaisedButton(
                              color: Theme.of(context).primaryColorDark,
                              textColor: Theme.of(context).primaryColorLight,
                              child: Text(
                                'Delete',
                                textScaleFactor: 1.5,
                              ),
                              onPressed: () {
                                setState(() {

                                  debugPrint("Delete button clicked");
                                  _delete();

                                });
                              },
                            ),

                          ),
                        ],
                      ),
                    ),

                  ]),
                ),
          ));
          }


      void moveToLastScreen() {
        Navigator.pop(context, true);
      }


}