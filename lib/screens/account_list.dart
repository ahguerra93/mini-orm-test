import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import '../models/user.dart';
import '../database/repository.dart';
import './account_detail.dart';
import '../models/account.dart';


class AccountList extends StatefulWidget {
      
     User user;

      AccountList(this.user);

      State<StatefulWidget> createState(){
            return AccountListState(this.user);
      }
}

class AccountListState extends State<AccountList> {
      
      Repository repository = Repository();
      List<Account> accountList;
      int count = 0;

      User user;
      AccountListState(this.user);

      @override
      Widget build(BuildContext context){

      if(accountList == null){
            accountList = List<Account>();
            updateListView();
      }

            return WillPopScope(

            onWillPop: () {

              moveToLastScreen();
            },
            child:  Scaffold(appBar: AppBar(
        title: Text("Accounts - ${user.name}"),
        leading: IconButton(icon: Icon(Icons.arrow_back),
                  onPressed:  () {
                    moveToLastScreen();
                  },),
      ),

      body: getAccountListView(),

      floatingActionButton: FloatingActionButton(
        onPressed: () {
          debugPrint('FAB clicked');
          navigateToDetail(Account(user.id, 1, 0.0, ''), 'Create Account');
        },
        tooltip: 'Add Note',

        child: Icon(Icons.add),
      ),
    )
    );
    }
  

  ListView getAccountListView() {

    TextStyle titleStyle = Theme.of(context).textTheme.subhead;
    print('PREPARIN LIST VIEW');

    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context,int position){
        return Card(
          color: Colors.white,
          elevation: 2.0,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              child: Icon(Icons.show_chart),
            ),
            title: Text(this.accountList[position].getAccountString(), style: titleStyle),

            subtitle: Text(this.accountList[position].id.toString()),

            trailing: GestureDetector(
              child: Icon(Icons.delete, color: Colors.grey,),
              onTap:  () {
                
                _delete(context, accountList[position]);
              },
            ),

            onTap: (){
              
              debugPrint("EDIT ListTile Tapped");
              navigateToDetail(this.accountList[position], "Edit Account");
              
            },

          ),
        );
      },
      );
  }

  void _delete(BuildContext context, Account account) async {

          debugPrint('Attempting to delete ${account.id}...');
          int result = await repository.deleteAccount(account);
          if (result != 0) {
                _showSnackBar(context, 'Account Deleted Successfully');
                updateListView();
          }
  }

  void _showSnackBar(BuildContext context, String message) {
          final snackBar = SnackBar(content: Text(message));
          Scaffold.of(context).showSnackBar(snackBar);
  }


  void navigateToDetail(Account account, String title) async {
          bool result = await Navigator.push(context, MaterialPageRoute(builder: (context) {
            return AccountDetail(account, title);
          }));


          if(result == true){
            updateListView();
          }
  }

  void updateListView() {
        print('UPDATING LIST VIEW....');
        final Future<Database> dbFuture = repository.initializeDatabase();
        dbFuture.then((database) {
          Future<List<Account>> accountListFuture = repository.getAccountFilteredList(user.id);
          //Future<List<Account>> accountListFuture = repository.getAccountList();
          accountListFuture.then((accountList) {
            setState(() {
                      this.accountList = accountList;
                      this.count = accountList.length;
                    });
          });
        });
  }
  void moveToLastScreen() {
        Navigator.pop(context, true);
  }
}

