import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import '../models/user.dart';
import '../database/repository.dart';
import './user_detail.dart';


class UserList extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return UserListState();
  }
}

class UserListState extends State<UserList> {
  Repository repository = Repository();
  List<User> userList;
  int count = 0;
  @override
  Widget build(BuildContext context) {
    
    if(userList == null){
      userList = List<User>();
      updateListView();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Users"),
      ),

      body: getUserListView(),

      floatingActionButton: FloatingActionButton(
        onPressed: () {
          debugPrint('FAB clicked');
          navigateToDetail(User('', 'm', ''), "Add User");
        },
        tooltip: 'Add User',

        child: Icon(Icons.add),
      ),
    );
  }

  ListView getUserListView() {

    TextStyle titleStyle = Theme.of(context).textTheme.subhead;

    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context,int position){
        return Card(
          color: Colors.white,
          elevation: 2.0,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              child: Icon(Icons.supervised_user_circle),
            ),
            title: Text(this.userList[position].name, style: titleStyle),

            subtitle: Text(getGender(this.userList[position].gender)),

            trailing: GestureDetector(
              child: Icon(Icons.delete, color: Colors.grey,),
              onTap:  () {
                
                _delete(context, userList[position]);
              },
            ),

            onTap: (){
              
              debugPrint("EDIT ListTile Tapped");
              navigateToDetail(this.userList[position], "Edit User");
              
            },

          ),
        );
      },
      );
  }
  String getGender(String genChar){
        
        var response = '';
        if(genChar == 'm'){
              response = 'Male';  
        }
        if(genChar == 'f'){
              response = 'Female';
        }
        return response;
  }
  void _delete(BuildContext context, User user) async {

          debugPrint('Attempting to delete ${user.name}...');
          int result = await repository.deleteUser(user);
          if (result != 0) {
                _showSnackBar(context, 'User Deleted Successfully');
                updateListView();
          }
  }

  void _showSnackBar(BuildContext context, String message) {
          final snackBar = SnackBar(content: Text(message));
          Scaffold.of(context).showSnackBar(snackBar);
  }


  void navigateToDetail(User user, String title) async {
          bool result = await Navigator.push(context, MaterialPageRoute(builder: (context) {
            return UserDetail(user, title);
          }));


          if(result == true){
            updateListView();
          }
  }

  void updateListView() {
        print('UPDATING LIST VIEW....');
        final Future<Database> dbFuture = repository.initializeDatabase();
        dbFuture.then((database) {
          // repository.createUserTable(database);
          // repository.createAccTypeTable(database);
          // repository.createAccountable(database);
          Future<List<User>> userListFuture = repository.getUserList();
          userListFuture.then((userList) {
            setState(() {
                      this.userList = userList;
                      this.count = userList.length;
                    });
          });
        });
        
  }

}
