class AccountType{

      int _id;
      String _description;

      AccountType(this._description);
      AccountType.withId(this._id,this._description);

      int get id => this._id;
      String get description => this._description;
      
      set description(String newDesc){
            if(newDesc.length <= 255) {
                  this._description = newDesc;
            }
      }

      Map<String, dynamic> toMap() {
  
            var map = Map<String, dynamic>();
    
            if(id != null){
                  map['id'] = _id;
            }

            map['desc'] = _description;


            return map;
      }



}