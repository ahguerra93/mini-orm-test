import '../database/repository.dart';
import '../models/account_type.dart';
import 'dart:async';

class Account {

      int _id;
      int _userId;
      int _accTypeId;
      double _amount;
      String _creationDate;

      Account(this._userId, this._accTypeId, this._amount, this._creationDate);
      Account.withId(this._id, this._userId, this._accTypeId, this._amount, this._creationDate);

      int get id => this._id;
      int get userId => this._userId;
      int get accTypeId => this._accTypeId;
      double get amount => this._amount;
      String get creationDate => this._creationDate;

      set amount(double newAmount){

            if(amount >= 0 ){
                  this._amount = newAmount;
            }     
      }

      set creationDate(String newDate){
            if(newDate.length <= 255) {
                  this._creationDate = newDate;
            }
      }

      Map<String, dynamic> toMap() {
  
            var map = Map<String, dynamic>();
      
            if(id != null){
                  map['id'] = _id;
            }

            map['user_id'] = _userId;
            map['account_id'] = _accTypeId;
            map['amount'] = _amount;
            map['creation_date'] = _creationDate;

            return map;
      }

       String getAccountString() {
            String result = '-';
            if(_accTypeId == 1){
              result = 'Regular';
            }

            if(_accTypeId == 2){
              result = 'Savings';
            }
            
            
            return result;

      }
      //  String getAccountString() {
      //       String result = '-';
      //       Repository repository = Repository();
            
      //       Future<AccountType> accTypeFuture = repository.getAccTypeWithId(accTypeId);
      //       accTypeFuture.then((accType){
      //           result = accType.description;
      //           return result;
      //       });
            
      //       return result;

      // }

      void updateAccountId(String nameString){

            Repository repository = Repository();
            Future<AccountType> accTypeFuture = repository.getAccTypeWithDesc(nameString);
            accTypeFuture.then((accType){
            _accTypeId = accType.id;
            });

      }







}