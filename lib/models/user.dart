class User {
  int _id;
  String _name;
  String _gender;
  String _birthDate;


  User(this._name, this._gender, this._birthDate);
  User.withId(this._id, this._name, this._gender, this._birthDate);

  int get id => this._id;
  String get name => this._name;
  String get gender => this._gender;
  String get birthDate => this._birthDate;


  set name(String newName){

          if(newName.length <= 255) {
          this._name = newName;
    }
  }

  set gender(String newGender){

          if(newGender.length <= 255) {
          this._gender = newGender;
    }
  }

  set birthDate(String newBirthDate){

          if(newBirthDate.length <= 255) {
          this._birthDate = newBirthDate;
    }
  }


  Map<String, dynamic> toMap() {
  
          var map = Map<String, dynamic>();
    
          if(id != null){
                map['id'] = _id;
          }

          map['name'] = _name;
          map['gender'] = _gender;
          map['birthdate'] = _birthDate;

          return map;
  }

  String getFullGender(){

          var result;
          if (this.gender == 'm'){

                result = 'Male';
          }
          if (this.gender == 'f'){
                result = 'Female';
          }

          return result;
  }

  void updateGenderWithString(String longString){

          var result = 'm';

          if(longString == 'Male') {

                  result = 'm';
          }

          if(longString == 'Female') {

                  result = 'f';
          }

          this.gender = result;
  }
  void updateName(String newName) {

          this.name = newName;
  }

}