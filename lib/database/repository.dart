import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';

import './tables/user_table.dart';
import './tables/account_type_table.dart';
import './tables/account_table.dart';
import '../models/user.dart';
import '../models/account_type.dart';
import '../models/account.dart';
import 'package:path_provider/path_provider.dart';



class Repository {

	static Repository _respository;    // Singleton DatabaseHelper
	static Database _database;                // Singleton Database
  UserTable userTable;

	Repository._createInstance(); // Named constructor to create instance of DatabaseHelper

	factory Repository() {

		if (_respository == null) {
			_respository = Repository._createInstance(); // This is executed only once, singleton object
		}
		return _respository;
	}

	Future<Database> get database async {

		if (_database == null) {
			_database = await initializeDatabase();
		}
		return _database;
	}


	Future<Database> initializeDatabase() async {

		// Get the directory path for both Android and iOS to store database.
    print('INITIALIZING DB!!!');
		Directory directory = await getApplicationDocumentsDirectory();
		String path = directory.path + '/newaccounts2.db';
    print(path);

		// Open/create the database at a given path
		var bankClientDatabase = await openDatabase(path, version: 1, onCreate: _createDb);
		return bankClientDatabase;
	}

	void _createDb(Database db, int newVersion) async {
    
    print('CREATING DB');

    UserTable.createUserTable(db);
    AccountTypeTable.createAccountTypeTable(db);
    AccountTypeTable.addData(db);
    AccountTable.createAccountTable(db);
    

  }

  void createUserTable(db){
        UserTable.createUserTable(db);
  }

  void createAccTypeTable(db) async{

        AccountTypeTable.createAccountTypeTable(db);
        

  }
  void createAccountable(db){
        AccountTable.createAccountTable(db);
  }
  

  
  

//######## USER OPERATIONS ###########

  //Method for DELETING ENTIRE USER TABLE calling our userTable object
  static void deleteUserTable(Database db) async{

        UserTable.deleteUserTable(db);      
  }
  
  //Method for INSERTING USER calling our userTable object
  Future<int> insertUser(User user) async {
		
    var result = UserTable.insert(database, user);
    
		return result;
	}

  //Method for DELETING USER calling our userTable object
    Future<int> deleteUser(User user) async {

        var result = UserTable.delete(database, user.id);    
        return result;
  }

  	Future<int> updateUser(User user) async {
	
		    var result = UserTable.update(database, user);
		    return result;
	}
  // Method or FETCHING ALL USERS OBJECTS calling our userTable object: 
	Future<List<User>> getUserList() async {

        Future<List<User>> userList;
        userList = UserTable.getList(database);
		
		    return userList;
	}
  // Method or GETTING THE NUMBER OF USER OBJECTS calling our userTable object:
  	Future<int> getUsersCount() async {
        
        var result = userTable.getCount(database);

        return result;
	}

  //######## ACCOUNT TYPE OPERATIONS ###########

  //Method for DELETING ENTIRE ACCOUNT TYPE TABLE calling our AccountTypeTable object
  static void deleteAccTypeTable(Database db) async{

        AccountTypeTable.deleteAccountTypeTable(db);
  }
  
  //Method for INSERTING ACCOUNT TYPE calling our AccountTypeTable object
  Future<int> insertAccType(AccountType accountType) async {
		
    var result = AccountTypeTable.insert(database, accountType);
    
		return result;
	}

  //Method for DELETING ACCOUNT TYPE calling our AccountTypeTable object
    Future<int> deleteAccType(AccountType accountType) async {

        var result = AccountTypeTable.delete(database, accountType.id);    
        return result;
  }

  	Future<int> updateAccType(AccountType accountType) async {
	
		    var result = AccountTypeTable.update(database, accountType);
		    return result;
	}
  // Method or FETCHING ALL ACCOUNT TYPE OBJECTS calling our AccountTypeTable object: 
	Future<List<AccountType>> getAccTypeList() async {

        Future<List<AccountType>> accTypeList;
        accTypeList = AccountTypeTable.getList(database);
		
		    return accTypeList;
	}

  // Method or FETCHING A SINGLE ACCOUNT TYPE OBJECT calling our AccountTypeTable object:

  Future<AccountType> getAccTypeWithId(int id) async {

        return AccountTypeTable.fetchAccType(database, "id = ?",id.toString());
  }

  Future<AccountType> getAccTypeWithDesc(String desc) async {

        return AccountTypeTable.fetchAccType(database, "description = ?",desc);
  }  
  // Method or GETTING THE NUMBER OF ACCOUNT TYPE OBJECTS calling our AccountTypeTable object:
  	Future<int> getAccTypeCount() async {
        
        var result = AccountTypeTable.getCount(database);

        return result;
	}

  //######## ACCOUNT OPERATIONS ###########

  //Method for DELETING ENTIRE ACCOUNT TABLE calling our AccountTable object
  static void deleteAccountTable(Database db) async{

        AccountTable.deleteAccountTable(db);
  }
  
  //Method for INSERTING ACCOUNT calling our AccountTable object
  Future<int> insertAccount(Account account) async {
		
    var result = AccountTable.insert(database, account);
    
		return result;
	}

  //Method for DELETING ACCOUNT calling our AccountTable object
    Future<int> deleteAccount(Account account) async {

        var result = AccountTable.delete(database, account.id);    
        return result;
  }

  	Future<int> updateAccount(Account account) async {
	
		    var result = AccountTable.update(database, account);
		    return result;
	}
  // Method or FETCHING ALL ACCOUNT OBJECTS calling our AccountTable object: 
	Future<List<Account>> getAccountList() async {

        Future<List<Account>> accountList;
        accountList = AccountTable.getList(database);
		
		    return accountList;
	}

  Future<List<Account>> getAccountFilteredList(int id) async {

        Future<List<Account>> accountList;
        accountList = AccountTable.getFilteredList(database, id);
		
		    return accountList;
	}
  

  // Method or FETCHING A SINGLE ACCOUNT OBJECT calling our AccountTable object:

  Future<Account> getAccount(int id) async {

        return AccountTable.fetchAccount(database, id);
  } 
  // Method or GETTING THE NUMBER OF ACCOUNT OBJECTS calling our AccountTable object:
  	Future<int> getAccountCount() async {
        
        var result = AccountTable.getCount(database);

        return result;
	}

}

