import 'package:minimized_orm/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';


class UserTable {

      static String _userTable = 'user_table';
      static String _colUserId = 'id';
      static String _colUserName = 'name';
      static String _colUserGender = 'gender';
      static String _colUserBirthDate = 'Birthdate';

      //method for creating the user table
      static void createUserTable(Database db) async {

            print('CREATE TABLE IF NOT EXISTS $_userTable('
            '$_colUserId INTEGER PRIMARY KEY AUTOINCREMENT, '
            '$_colUserName TEXT, '
            '$_colUserGender TEXT, $_colUserBirthDate TEXT);');

            // attempting to create db

            await db.execute('CREATE TABLE IF NOT EXISTS $_userTable('
            '$_colUserId INTEGER PRIMARY KEY AUTOINCREMENT, '
            '$_colUserName TEXT, '
            '$_colUserGender TEXT, $_colUserBirthDate TEXT);');
      }

      //method for deleting the user table
      static void deleteUserTable(Database db) async{

            print('Attempting to delete $_userTable table.....');
            await db.execute('DROP TABLE IF EXISTS  $_userTable;');
            print('Table USERS DELETED');
      }

      //method for inserting a user record to database
      static Future<int> insert(Future<Database> database, User user) async {
            Database db = await database;
            var result = await db.insert(_userTable, user.toMap());
            return result;
	    }

      //method for deleting a user from database
      static Future<int> delete(Future<Database> database, int id) async {
            var db = await database;
            int result = await db.rawDelete('DELETE FROM $_userTable WHERE $_colUserId = $id');
            return result;
      }

      //method for updating a user from database
      static Future<int> update(Future<Database> database, User user) async {
            var db = await database;
            var result = await db.update(_userTable, user.toMap(), where: '$_colUserId = ?', whereArgs: [user.id]);
            return result;
	    }

      //Method for selecting all users from database
      static Future<List<User>> getList(Future<Database> database) async {
		        Database db = await database;

//	      	var result = await db.rawQuery('SELECT * FROM $tableName order by $colPriority ASC');
            var resultMap;
          
            resultMap = await db.query(_userTable);
            
            var result = getUserList(resultMap);
            return result;
	    }

      static Future<List<User>> getUserList(List<Map<String, dynamic>> map) async {

            List<Map<String, dynamic>> userMapList = map; // Get 'Map List' from database
            int count = userMapList.length;         // Count the number of map entries in db table

            List<User> userList = List<User>();
            // For loop to create a 'User List' from a 'Map List'
            for (int i = 0; i < count; i++) {
              userList.add(fromMapObject(userMapList[i]));
            }

            return userList;
          }

          Future<int> getCount(Future<Database> database) async {
                Database db = await database;
                List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $_userTable');
                int result = Sqflite.firstIntValue(x);

                return result;
	        }

          static fromMapObject(Map<String, dynamic> map) {
                
                User user = User.withId(map['id'], map['name'], map['gender'], map['birthdate']);
                return user;
          
          }

}

       





      



  




