import 'package:sqflite/sqflite.dart';
import 'dart:async';

import 'package:minimized_orm/models/account.dart';

class AccountTable {

      //user table names
      static String _userTable = 'user_table'; 
      static String _colUserUserId = 'id';
      //account type table names
      static String _accountTypeTable = 'account_type_table';
      static String _colAccountAccountTypeId = 'id';
      //acount table names
      static String _accountTable = 'user_account_table';     
      static String _colAccountId = 'id';
      static String _colUserId = 'user_id';
      static String _colAccountTypeId = 'account_id';
      static String _colAccountAmount = 'amount';
      static String _colAccountDate = 'creation_date';

      //method for creating the user table
      static void createAccountTable(Database db) async {


    
            print('CREATE TABLE IF NOT EXISTS $_accountTable('
            '$_colAccountId INTEGER PRIMARY KEY AUTOINCREMENT, '
            '$_colUserId INTEGER, '
            '$_colAccountTypeId INTEGER, '
            '$_colAccountAmount REAL, '
            '$_colAccountDate TEXT, '
            'FOREIGN KEY ($_colUserId) REFERENCES $_userTable($_colUserUserId), '
            'FOREIGN KEY ($_colAccountTypeId) REFERENCES $_accountTypeTable($_colAccountAccountTypeId));');

            // attempting to create tables

            await db.execute('CREATE TABLE IF NOT EXISTS $_accountTable('
            '$_colAccountId INTEGER PRIMARY KEY AUTOINCREMENT, '
            '$_colUserId INTEGER, '
            '$_colAccountTypeId INTEGER, '
            '$_colAccountAmount REAL, '
            '$_colAccountDate TEXT, '
            'FOREIGN KEY ($_colUserId) REFERENCES $_userTable($_colUserUserId), '
            'FOREIGN KEY ($_colAccountTypeId) REFERENCES $_accountTypeTable($_colAccountAccountTypeId));');
      }

      //method for deleting the account type table
      static void deleteAccountTable(Database db) async{

            print('Attempting to delete $_accountTable table.....');
            await db.execute('DROP TABLE IF EXISTS  $_accountTable;');
            print('Table $_accountTable DELETED');
      }

      //method for inserting an account record to database
      static Future<int> insert(Future<Database> database, Account account) async {
            Database db = await database;
            var result = await db.insert(_accountTable, account.toMap());
            return result;
	    }

      //method for deleting an account from database
      static Future<int> delete(Future<Database> database, int id) async {
            var db = await database;
            int result = await db.rawDelete('DELETE FROM $_accountTable WHERE $_colAccountId = $id');
            return result;
      }

      //method for updating an account from database
      static Future<int> update(Future<Database> database, Account account) async {
            var db = await database;
            var result = await db.update(_accountTable, account.toMap(), where: '$_colAccountId = ?', whereArgs: [account.id]);
            return result;
	    }
      static Future<Account> fetchAccount(Future<Database> database, int id) async {
            var db = await database;
            List<Map> results = await db.query(_accountTable, where: "id = ?", whereArgs: [id]);

            Account account = fromMapObject(results[0]);

            return account;

      }

            //Method for selecting all accounts from database
      static Future<List<Account>> getFilteredList(Future<Database> database, int id) async {
		        Database db = await database;

//	      	var result = await db.rawQuery('SELECT * FROM $tableName order by $colPriority ASC');

            var resultMap;
            print('Attempting to get query!!!!!!!!!!!!!');
            resultMap = await db.query(_accountTable, where: "$_colUserId = ?", whereArgs: [id]);
            
            var result = getAccountList(resultMap);
            return result;
	    }
      
      //Method for selecting all accounts from database
      static Future<List<Account>> getList(Future<Database> database) async {
		        Database db = await database;

//	      	var result = await db.rawQuery('SELECT * FROM $tableName order by $colPriority ASC');
            var resultMap;
            print('Attempting to get query!!!!!!!!!!!!!');
            resultMap = await db.query(_accountTable);
            
            var result = getAccountList(resultMap);
            return result;
	    }



      static Future<List<Account>> getAccountList(List<Map<String, dynamic>> map) async {

            List<Map<String, dynamic>> accountMapList = map; // Get 'Map List' from database
            int count = accountMapList.length;         // Count the number of map entries in db table

            List<Account> accountList = List<Account>();
            // For loop to create an 'Account List' from a 'Map List'
            for (int i = 0; i < count; i++) {
              accountList.add(fromMapObject(accountMapList[i]));
            }

            return accountList;
          }

          static Future<int> getCount(Future<Database> database) async {
                Database db = await database;
                List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $_accountTable');
                int result = Sqflite.firstIntValue(x);

                return result;
	        }

          static fromMapObject(Map<String, dynamic> map) {
                
                Account account = Account.withId(map[_colAccountId], map[_colUserId], map[_colAccountTypeId], map[_colAccountAmount], map[_colAccountDate]);
                return account;
          
          }      
}