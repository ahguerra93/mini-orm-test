import 'package:sqflite/sqflite.dart';
import 'dart:async';

import 'package:minimized_orm/models/account_type.dart';

class AccountTypeTable {

      static String _accountTypeTable = 'account_type_table';
      static String _colAccTypeId = 'id';
      static String _colAccTypeDesc = 'description';

      //method for creating the user table
      static void createAccountTypeTable(Database db) async {

            print('CREATE TABLE IF NOT EXISTS $_accountTypeTable('
            '$_colAccTypeId INTEGER PRIMARY KEY AUTOINCREMENT, '
            '$_colAccTypeDesc TEXT);');

            // attempting to create tables

            await db.execute('CREATE TABLE IF NOT EXISTS $_accountTypeTable('
            '$_colAccTypeId INTEGER PRIMARY KEY AUTOINCREMENT, '
            '$_colAccTypeDesc TEXT);'
            );
      }
      static void addData(Database db) async{
            await db.execute(
            'INSERT INTO $_accountTypeTable ($_colAccTypeDesc)'
            'VALUES("Regular"),("Savings");'
            );
      }

      //method for deleting the account type table
      static void deleteAccountTypeTable(Database db) async{

            print('Attempting to delete $_accountTypeTable table.....');
            await db.execute('DROP TABLE IF EXISTS  $_accountTypeTable;');
            print('Table $_accountTypeTable DELETED');
      }

      //method for inserting an account type record to database
      static Future<int> insert(Future<Database> database, AccountType accountType) async {
            Database db = await database;
            var result = await db.insert(_accountTypeTable, accountType.toMap());
            return result;
	    }

      //method for deleting an account type from database
      static Future<int> delete(Future<Database> database, int id) async {
            var db = await database;
            int result = await db.rawDelete('DELETE FROM $_accountTypeTable WHERE $_colAccTypeId = $id');
            return result;
      }

      //method for updating an account type from database
      static Future<int> update(Future<Database> database, AccountType accType) async {
            var db = await database;
            var result = await db.update(_accountTypeTable, accType.toMap(), where: '$_colAccTypeId = ?', whereArgs: [accType.id]);
            return result;
	    }
      static Future<AccountType> fetchAccType(Future<Database> database, String whereString, String arg) async {
            var db = await database;
            List<Map> results = await db.query(_accountTypeTable, where: whereString, whereArgs: [arg]);

            AccountType accType = fromMapObject(results[0]);

            return accType;

      }
      //Method for selecting all account types from database
      static Future<List<AccountType>> getList(Future<Database> database) async {
		        Database db = await database;

//	      	var result = await db.rawQuery('SELECT * FROM $tableName order by $colPriority ASC');
            var resultMap;
          
            resultMap = await db.query(_accountTypeTable);
            
            var result = getAccountTypeList(resultMap);
            return result;
	    }

      static Future<List<AccountType>> getAccountTypeList(List<Map<String, dynamic>> map) async {

            List<Map<String, dynamic>> accTypeMapList = map; // Get 'Map List' from database
            int count = accTypeMapList.length;         // Count the number of map entries in db table

            List<AccountType> accTypeList = List<AccountType>();
            // For loop to create a 'User List' from a 'Map List'
            for (int i = 0; i < count; i++) {
              accTypeList.add(fromMapObject(accTypeMapList[i]));
            }

            return accTypeList;
          }

          static Future<int> getCount(Future<Database> database) async {
                Database db = await database;
                List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $_accountTypeTable');
                int result = Sqflite.firstIntValue(x);

                return result;
	        }

          static fromMapObject(Map<String, dynamic> map) {
                
                AccountType accType = AccountType.withId(map['id'], map['description']);
                return accType;
          
          }      
}